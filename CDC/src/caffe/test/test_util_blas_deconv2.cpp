// Copyright 2014 BVLC and contributors.

#include <cstring>

#include "cuda_runtime.h"
#include "cublas_v2.h"

#include "gtest/gtest.h"
#include "caffe/blob.hpp"
#include "caffe/util/math_functions.hpp"

#include "caffe/test/test_caffe_main.hpp"

namespace caffe {

extern cudaDeviceProp CAFFE_TEST_CUDA_PROP;

typedef ::testing::Types<float, double> Dtypes;

template <typename Dtype>
class TransTest : public ::testing::Test {};

TYPED_TEST_CASE(TransTest, Dtypes);

TYPED_TEST(TransTest, TestGemm) {
  Blob<TypeParam> A(1, 1, 2, 3);
  Blob<TypeParam> B(1, 1, 3, 4);
  Blob<TypeParam> C(1, 1, 2, 4);
  TypeParam data[12] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
  TypeParam A_reshape_data[6] = {1, 4, 2, 5, 3, 6};
  TypeParam B_reshape_data[12] = {1, 5, 9, 2, 6, 10, 3, 7, 11, 4, 8, 12};
  TypeParam result[8] = {38, 44, 50, 56, 83, 98, 113, 128};
  memcpy(A.mutable_cpu_data(), data, 6 * sizeof(TypeParam));
  memcpy(B.mutable_cpu_data(), data, 12 * sizeof(TypeParam));
  

  if (sizeof(TypeParam) == 4 || CAFFE_TEST_CUDA_PROP.major >= 2) {
	
	// Test how transpose work in cblas
    /* notes page 8
	Blob<TypeParam> A1(1, 3, 2, 4);
	Blob<TypeParam> B1(1, 1, 6, 1);
	Blob<TypeParam> C1(1, 1, 4, 1);
	TypeParam A1_data[24] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
	TypeParam B1_data[6] = {1, 2, 3, 4, 5, 6};
	// TypeParam C1_data[12] = {1, 2, 3, 4, 5, 6};
	memcpy(A1.mutable_cpu_data(), A1_data, 24 * sizeof(TypeParam));
	memcpy(B1.mutable_cpu_data(), B1_data, 6 * sizeof(TypeParam));
    caffe_cpu_gemm<TypeParam>(CblasTrans, CblasNoTrans, 4, 1, 6, 1.,
        A1.cpu_data(), B1.cpu_data(), 0., C1.mutable_cpu_data());
	LOG(ERROR) << C1.cpu_data()[0];
		
    caffe_gpu_gemm<TypeParam>(CblasTrans, CblasNoTrans, 4, 1, 6, 1.,
        A1.gpu_data(), B1.gpu_data(), 0., C1.mutable_gpu_data());
	LOG(ERROR) << C1.cpu_data()[0];
	
	// should be 1+10+27+52+95+126=311 (3,2,4)->Trans->(4,3,2) rather than 1+18+57+20+65+126=287 (3,2,4)->Trans->(4,2,3)
	// final result is 301. so means here we cannot transpose according to the last/lowest dim
	
	*/
	
	// here we test (3,2,4)->Trans->(2,4,3) or (3,2,4)->Trans->(4,2,3)
	// notes page 9
	Blob<TypeParam> A1(1, 3, 2, 4);
	Blob<TypeParam> B1(1, 1, 3, 1);
	Blob<TypeParam> C1(1, 1, 8, 1);
	Blob<TypeParam> D1(1, 1, 1, 1);
	Blob<TypeParam> E1(1, 1, 1, 9);
	TypeParam A1_data[24] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
	TypeParam B1_data[3] = {1, 2, 3};
	memcpy(A1.mutable_cpu_data(), A1_data, 24 * sizeof(TypeParam));
	memcpy(B1.mutable_cpu_data(), B1_data, 3 * sizeof(TypeParam));
	
    caffe_cpu_gemm<TypeParam>(CblasTrans, CblasNoTrans, 8, 1, 3, 1.,
        A1.cpu_data(), B1.cpu_data(), 0., C1.mutable_cpu_data());
	LOG(ERROR) << C1.cpu_data()[1];
		
    caffe_gpu_gemm<TypeParam>(CblasTrans, CblasNoTrans, 8, 1, 3, 1.,
        A1.gpu_data(), B1.gpu_data(), 0., C1.mutable_gpu_data());
	LOG(ERROR) << C1.cpu_data()[1];
	
    caffe_cpu_gemm<TypeParam>(CblasTrans, CblasNoTrans, 1, 1, 3, 1.,
        B1.cpu_data(), B1.cpu_data(), 0., D1.mutable_cpu_data());
	LOG(ERROR) << D1.cpu_data()[0];
		
    caffe_gpu_gemm<TypeParam>(CblasNoTrans, CblasTrans, 3, 3, 1, 1.,
        B1.gpu_data(), B1.gpu_data(), 0., E1.mutable_gpu_data());
	LOG(ERROR) << E1.cpu_data()[8];
	
	// should be (2,10,18).*(1,2,3)=76 (3,2,4)->Trans->(4,3,2) rather than (5,13,21).*(1,2,3)=94 (3,2,4)->Trans->(4,2,3)
	// result is 76 as expected. conclusion: using transpose to put the highest dim to the lowest, will not change dim order of the rest dims.
	
    /*
	// [1, 2, 3; 4 5 6] * [1, 2, 3, 4; 5, 6, 7, 8; 9, 10, 11, 12];
    caffe_cpu_gemm<TypeParam>(CblasNoTrans, CblasNoTrans, 2, 4, 3, 1.,
        A.cpu_data(), B.cpu_data(), 0., C.mutable_cpu_data());
    for (int i = 0; i < 8; ++i) {
      EXPECT_EQ(C.cpu_data()[i], result[i]);
    }
    caffe_gpu_gemm<TypeParam>(CblasNoTrans, CblasNoTrans, 2, 4, 3, 1.,
        A.gpu_data(), B.gpu_data(), 0., C.mutable_gpu_data());
    for (int i = 0; i < 8; ++i) {
      EXPECT_EQ(C.cpu_data()[i], result[i]);
    }

    // Test when we have a transposed A
    A.Reshape(1, 1, 3, 2);
    memcpy(A.mutable_cpu_data(), A_reshape_data, 6 * sizeof(TypeParam));
    caffe_cpu_gemm<TypeParam>(CblasTrans, CblasNoTrans, 2, 4, 3, 1.,
        A.cpu_data(), B.cpu_data(), 0., C.mutable_cpu_data());
    for (int i = 0; i < 8; ++i) {
      EXPECT_EQ(C.cpu_data()[i], result[i]);
    }
    caffe_gpu_gemm<TypeParam>(CblasTrans, CblasNoTrans, 2, 4, 3, 1.,
        A.gpu_data(), B.gpu_data(), 0., C.mutable_gpu_data());
    for (int i = 0; i < 8; ++i) {
      EXPECT_EQ(C.cpu_data()[i], result[i]);
    }

    // Test when we have a transposed A and a transposed B too
    B.Reshape(1, 1, 4, 3);
    memcpy(B.mutable_cpu_data(), B_reshape_data, 12 * sizeof(TypeParam));
    caffe_cpu_gemm<TypeParam>(CblasTrans, CblasTrans, 2, 4, 3, 1.,
        A.cpu_data(), B.cpu_data(), 0., C.mutable_cpu_data());
    for (int i = 0; i < 8; ++i) {
      EXPECT_EQ(C.cpu_data()[i], result[i]);
    }
    caffe_gpu_gemm<TypeParam>(CblasTrans, CblasTrans, 2, 4, 3, 1.,
        A.gpu_data(), B.gpu_data(), 0., C.mutable_gpu_data());
    for (int i = 0; i < 8; ++i) {
      EXPECT_EQ(C.cpu_data()[i], result[i]);
    }

    // Test when we have a transposed B
    A.Reshape(1, 1, 2, 3);
    memcpy(A.mutable_cpu_data(), data, 6 * sizeof(TypeParam));
    caffe_cpu_gemm<TypeParam>(CblasNoTrans, CblasTrans, 2, 4, 3, 1.,
        A.cpu_data(), B.cpu_data(), 0., C.mutable_cpu_data());
    for (int i = 0; i < 8; ++i) {
      EXPECT_EQ(C.cpu_data()[i], result[i]);
    }
    caffe_gpu_gemm<TypeParam>(CblasNoTrans, CblasTrans, 2, 4, 3, 1.,
        A.gpu_data(), B.gpu_data(), 0., C.mutable_gpu_data());
    for (int i = 0; i < 8; ++i) {
      EXPECT_EQ(C.cpu_data()[i], result[i]);
    }
    */
  } else {
    LOG(ERROR) << "Skipping test due to old architecture.";	// LOG(ERROR) can output result but LOG(INFO) cannot in this file
  }
}

/*
TYPED_TEST(TransTest, TestGemv) {
  Blob<TypeParam> A(1, 1, 2, 3);
  Blob<TypeParam> x(1, 1, 1, 3);
  Blob<TypeParam> y(1, 1, 1, 2);
  TypeParam data[6] = {1, 2, 3, 4, 5, 6};
  TypeParam result_2[2] = {14, 32};
  TypeParam result_3[3] = {9, 12, 15};
  memcpy(A.mutable_cpu_data(), data, 6 * sizeof(TypeParam));
  memcpy(x.mutable_cpu_data(), data, 3 * sizeof(TypeParam));

  if (sizeof(TypeParam) == 4 || CAFFE_TEST_CUDA_PROP.major >= 2) {
    caffe_cpu_gemv<TypeParam>(CblasNoTrans, 2, 3, 1., A.cpu_data(),
        x.cpu_data(), 0., y.mutable_cpu_data());
    for (int i = 0; i < 2; ++i) {
      EXPECT_EQ(y.cpu_data()[i], result_2[i]);
    }
    caffe_gpu_gemv<TypeParam>(CblasNoTrans, 2, 3, 1., A.gpu_data(),
        x.gpu_data(), 0., y.mutable_gpu_data());
    for (int i = 0; i < 2; ++i) {
      EXPECT_EQ(y.cpu_data()[i], result_2[i]);
    }

    // Test transpose case
    memcpy(y.mutable_cpu_data(), data, 2 * sizeof(TypeParam));
    caffe_cpu_gemv<TypeParam>(CblasTrans, 2, 3, 1., A.cpu_data(),
        y.cpu_data(), 0., x.mutable_cpu_data());
    for (int i = 0; i < 3; ++i) {
      EXPECT_EQ(x.cpu_data()[i], result_3[i]);
    }
    caffe_gpu_gemv<TypeParam>(CblasTrans, 2, 3, 1., A.gpu_data(),
        y.gpu_data(), 0., x.mutable_gpu_data());
    for (int i = 0; i < 3; ++i) {
      EXPECT_EQ(x.cpu_data()[i], result_3[i]);
    }
  } else {
    LOG(ERROR) << "Skipping test due to old architecture.";
  }
}
*/

}  // namespace caffe
