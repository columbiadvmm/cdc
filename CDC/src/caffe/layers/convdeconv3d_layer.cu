/*
 *
 *  When: 2016/09/04
 *  Who: Zheng Shou
 *  Where: Columbia University
 *
 */


#include <vector>
#include "caffe/layer.hpp"
#include "caffe/convdeconv3d_layer.hpp"
#include "caffe/util/vol2col.hpp"
#include "caffe/util/swap_blob_array_channel_length.hpp"
#include "caffe/filler.hpp"
#include "caffe/util/math_functions.hpp"

namespace caffe {

template <typename Dtype>
Dtype Convdeconv3DLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      vector<Blob<Dtype>*>* top) {
		  
  const Dtype* bottom_data = bottom[0]->gpu_data();
  Dtype* top_data = (*top)[0]->mutable_gpu_data();
  Dtype* col_data = col_buffer_.mutable_gpu_data();
  Dtype* swap_data = swap_buffer_.mutable_gpu_data();
  const Dtype* weight = this->blobs_[0]->gpu_data();		  
	  
  for (int n = 0; n < num_; ++n) {
	  
	// bottom_data (1,c,IL,IH=KW,IW=KW) -> swap_data (1,IL,c,IH=KW,IW=KW)
	swap_blob_array_channel_length_gpu(bottom_data + bottom[0]->offset(n), channels_, length_, height_, width_, swap_data);
	
	// col_data (1, num_output_*KL, IL, 1, 1) = (num_output_*KL)*(IL)
	// weight (num_output_,KL,c,KH,KW)=(num_output_*KL)*(c*KH*KW)
	// x 
	// swap_data (1,IL,c,IH=KW,IW=KW)=(IL)*(c*KH*KW)->Trans->(c*KH*KW)*(IL)
    caffe_gpu_gemm<Dtype>(CblasNoTrans, CblasTrans, num_output_*kernel_depth_, length_, channels_*kernel_size_*kernel_size_,
    (Dtype)1., weight, swap_data,
    (Dtype)0., col_data);

    // col2vol from col_data (1,num_output_*KL,IL,1,1) -> top_data (num_output_*OL)
	// treat as deconv with size=1 in H and W for both input and output. 
    col2vol_gpu(col_data, num_output_, length_out_, 1, 1,
    1, kernel_depth_, 0, temporal_pad_, 1, temporal_stride_,
    top_data + (*top)[0]->offset(n));

    // third, add bias
    if (bias_term_) {
      caffe_gpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, num_output_,
      N0_, 1, (Dtype)1., this->blobs_[1]->gpu_data(),
      reinterpret_cast<const Dtype*>(bias_multiplier_->gpu_data()),
      (Dtype)1., top_data + (*top)[0]->offset(n));
    }
  }
	  return Dtype(0.);
}

template <typename Dtype>
void Convdeconv3DLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
      const bool propagate_down, vector<Blob<Dtype>*>* bottom) {
		  
  const Dtype* top_diff = top[0]->gpu_diff();
  const Dtype* weight = this->blobs_[0]->gpu_data();
  Dtype* weight_diff = this->blobs_[0]->mutable_gpu_diff();
  const Dtype* bottom_data = (*bottom)[0]->gpu_data();
  Dtype* bottom_diff = (*bottom)[0]->mutable_gpu_diff();
  Dtype* col_data = col_buffer_.mutable_gpu_data();
  Dtype* col_diff = col_buffer_.mutable_gpu_diff();
  Dtype* swap_data = swap_buffer_.mutable_gpu_data();
  Dtype* swap_diff = swap_buffer_.mutable_gpu_diff();
  // bias gradient if necessary
  Dtype* bias_diff = NULL;

  if (bias_term_) {
    bias_diff = this->blobs_[1]->mutable_gpu_diff();
    CUDA_CHECK(cudaMemset(bias_diff, 0, sizeof(Dtype) * this->blobs_[1]->count()));
    for (int n = 0; n < num_; ++n) {
	  caffe_gpu_gemv<Dtype>(CblasNoTrans, num_output_, N0_,
		  1., top_diff + top[0]->offset(n),
		  reinterpret_cast<const Dtype*>(bias_multiplier_->gpu_data()), 1.,
		  bias_diff);
    }
  }
		  
  CUDA_CHECK(cudaMemset(weight_diff, 0, sizeof(Dtype) * this->blobs_[0]->count()));
  for (int n = 0; n < num_; ++n) {
	// since we saved memory in the forward pass by not storing all swap data,
	// we will need to recompute them.
	
	// bottom_data (1,c,IL,IH=KW,IW=KW) -> swap_data (1,IL,c,IH=KW,IW=KW)
	swap_blob_array_channel_length_gpu(bottom_data + (*bottom)[0]->offset(n), channels_, length_, height_, width_, swap_data);

	// gradient w.r.t. weight. Note that we will accumulate diffs.
	
	// vol2col from top_diff (num_output_*OL) -> col_diff (1,num_output_*KL,IL,1,1)
	// treat as deconv with size=1 in H and W for both input and output.
	vol2col_gpu(top_diff + top[0]->offset(n), num_output_, length_out_, 1, 1,
	  	    1, kernel_depth_, 0, temporal_pad_, 1, temporal_stride_, col_diff);

	// weight_diff (num_output_,KL,c,KH,KW)=(num_output_*KL)*(c*KH*KW)
	// col_diff (1, num_output_*KL, IL, 1, 1) = (num_output_*KL)*(IL)
	// x 
	// swap_data (1,IL,c,IH=KW,IW=KW)=(IL)*(c*KH*KW)
	caffe_gpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, num_output_*kernel_depth_, channels_*kernel_size_*kernel_size_, length_,
				  (Dtype)1., col_diff,
				  swap_data, (Dtype)1.,
				  weight_diff);

	  // gradient w.r.t. bottom data, if necessary
	  if (propagate_down) {
		  // swap_diff = col_diff x weight
		  // swap_diff (1,IL,c,IH=KW,IW=KW)=(IL)*(c*KH*KW)
		  // col_diff (1, num_output_*KL, IL, 1, 1) = (num_output_*KL)*(IL)->Trans->(IL)*(num_output_*KL)
		  // x 
		  // weight (num_output_,KL,c,KH,KW)=(num_output_*KL)*(c*KH*KW)
		  caffe_gpu_gemm<Dtype>(CblasTrans, CblasNoTrans, length_, channels_*kernel_size_*kernel_size_, num_output_*kernel_depth_,
				  (Dtype)1., col_diff, weight,
				  (Dtype)0., swap_diff);
				  
		  // swap_diff (1,IL,c,IH=KW,IW=KW) -> bottom_diff (1,c,IL,IH=KW,IW=KW)
		  swap_blob_array_channel_length_gpu(swap_diff, length_, channels_, height_, width_, bottom_diff + (*bottom)[0]->offset(n));
	  }

  }
  
}


INSTANTIATE_CLASS(Convdeconv3DLayer);

}  // namespace caffe
